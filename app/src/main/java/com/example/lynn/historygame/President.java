package com.example.lynn.historygame;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import static com.example.lynn.historygame.MainActivity.*;

/**
 * Created by lynn on 6/15/2016.
 */
public class President {
    private String name;
    private String startDate;
    private String endDate;
    private int id;

    public President(String name,
                     String startDate,
                     String endDate,
                     int id) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.id = id;
    }

    public static List<President> getPresidents() {
        Cursor cursor = database.rawQuery("SELECT * FROM presidents;",new String[]{});

        cursor.moveToFirst();

        List<President> output = new ArrayList<>();

        do {
            int nameIndex = cursor.getColumnIndex("president");
            int startDateIndex = cursor.getColumnIndex("startdate");
            int endDateIndex = cursor.getColumnIndex("enddate");
            int idIndex = cursor.getColumnIndex("drawable");

            String name = cursor.getString(nameIndex);
            String startDate = cursor.getString(startDateIndex);
            String endDate = cursor.getString(endDateIndex);
            int id = cursor.getInt(idIndex);

            output.add(new President(name,startDate,endDate,id));
        } while (cursor.moveToNext());

        return(output);
    }

    public String getName() {
        return(name);
    }

    public String getStartDate() {
        return(startDate);
    }

    public String getEndDate() {
        return(endDate);
    }

    public int getId() {
        return(id);
    }



}
