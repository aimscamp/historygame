package com.example.lynn.historygame;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import static com.example.lynn.historygame.MainActivity.*;

/**
 * Created by lynn on 6/14/2016.
 */
public class GamePanel extends TableLayout {

    public GamePanel(Context context)
    {
        super(context);

        views = new ImageView[4];

        textViews = new TextView[4];

        TableRow row = new TableRow(context);

        for (int counter=0;counter<views.length;counter++) {
            views[counter] = new ImageView(context);

            views[counter].setLayoutParams(new TableRow.LayoutParams(width / views.length, (3*height)/8));

            views[counter].setBackground(getResources().getDrawable(R.drawable.border));

            row.addView(views[counter]);
        }

        TableRow row1 = new TableRow(context);

        for (int counter=0;counter<textViews.length;counter++) {
            textViews[counter] = new TextView(context);

            textViews[counter].setLayoutParams(new TableRow.LayoutParams(width / views.length, (3*height)/8));

            textViews[counter].setBackground(getResources().getDrawable(R.drawable.border1));

            textViews[counter].setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

            textViews[counter].setTextSize(40);

            textViews[counter].setOnClickListener(listener);

            row1.addView(textViews[counter]);
        }

        addView(row);
        addView(row1);
    }

}
